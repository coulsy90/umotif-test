# uMotif-ui

To setup the project please run the following commands. This should start up the application
on ``http://localhost:3000/``. It will need to be on port 3000 to work correctly.

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```
