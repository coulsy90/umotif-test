# uMotif

You will need to create a database and update the `.env` file with the database credentials
in order for the application to work.

You will also need to run the following commands: 

## Project Setup

```sh
composer install
```

### Run the migrations to create the database

```sh
php artisan migrate
```

### Compile and Hot-Reload for Development

```sh
php artisan serve
```

This should start up the application
on ``http://127.0.0.1:8000``. It will need to be on port 8000 to work correctly.
