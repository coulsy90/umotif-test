<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScreeningEligibility extends Model
{
    use HasFactory;

    protected string $table = 'screening_eligibility';

    protected array $fillable = [
        'response_id',
        'first_name',
        'cohort',
    ];
}
