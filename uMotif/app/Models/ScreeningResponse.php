<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScreeningResponse extends Model
{
    use HasFactory;

    protected array $fillable = [
        'first_name',
        'date_of_birth',
        'migraine_frequency',
        'daily_frequency',
    ];
}
