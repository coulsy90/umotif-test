<?php

namespace App\Http\Controllers;

use App\Http\Requests\ScreeningPostRequest;
use App\Services\ScreeningService;
use Illuminate\Http\JsonResponse;

class ScreeningController extends Controller
{
    protected ScreeningService $screeningService;

    public function __construct(ScreeningService $screeningService)
    {
        $this->screeningService = $screeningService;
    }

    public function submit(ScreeningPostRequest $request): JsonResponse
    {
        $eligibility = $this->screeningService->storeScreeningEligibility(
            $this->screeningService->createScreeningResponse($request)
        );

        if (!empty($eligibility)) {
            return response()->json(
                [
                    'success' => 'Screening response successful',
                    'message' => $this->screeningService->screeningResponseMessage($eligibility),
                ]
            );
        }

        return response()->json(['error' => 'Error submitting screening response']);
    }
}
