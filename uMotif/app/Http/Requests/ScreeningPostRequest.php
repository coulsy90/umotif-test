<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScreeningPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'first_name' => 'required|max:255',
            'dob' => 'required|date',
            'migraine_frequency' => 'required',
        ];

        if ($this->request->get('migraine_frequency') === 'Daily') {
            $rules['daily_frequency'] = ['required'];
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'first_name.required' => 'Your first name is required.',
            'dob.required' => 'Your date of birth is required.',
            'migraine_frequency.required' => 'You migraine frequency is required.'
        ];
    }
}
