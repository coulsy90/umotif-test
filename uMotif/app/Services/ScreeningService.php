<?php

namespace App\Services;

use App\Models\ScreeningEligibility;
use App\Models\ScreeningResponse;

class ScreeningService
{
    public function __construct()
    {

    }

    public function createScreeningResponse($request): ScreeningResponse
    {
        return ScreeningResponse::create([
            'first_name' => $request->first_name,
            'date_of_birth' => $request->dob,
            'migraine_frequency' => $request->migraine_frequency,
            'daily_frequency' => $request->daily_frequency ?: 'n/a',
        ]);
    }

    public function storeScreeningEligibility(ScreeningResponse $response): ScreeningEligibility
    {
        $cohort = 'n/a';

        if ($this->calculateAge($response->date_of_birth) > 17) {
            $cohort = $response->migraine_frequency === 'Daily' ? 'Cohort B' : 'Cohort A';
        }

        return ScreeningEligibility::create([
            'response_id' => $response->id,
            'first_name' => $response->first_name,
            'cohort' => $cohort
        ]);
    }

    public function calculateAge($dob): int
    {
        $age = date_diff(date_create($dob), date_create(date('d-m-Y')));

        return intval($age->format('%y'));
    }

    public function screeningResponseMessage(ScreeningEligibility $eligibility): string
    {
        if ($eligibility->cohort === 'n/a') {
            return 'Participants must be over 18 years of age';
        }

        return sprintf(
            'Participant %s is assigned to %s',
            $eligibility->first_name,
            $eligibility->cohort
        );
    }
}
