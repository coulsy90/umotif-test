<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('screening_eligibility', function (Blueprint $table) {
            $table->id();
            $table->integer('response_id')->unsigned();
            $table->string('first_name');
            $table->enum('cohort', ['cohort A', 'cohort B', 'n/a']);
            $table->timestamps();
            $table->foreign('response_id')->references('id')->on('screening_responses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('screening_eligibility');
    }
};
