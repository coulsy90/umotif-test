<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('screening_responses', function (Blueprint $table) {
            $table->integer('id')->unsigned()->autoIncrement();
            $table->string('first_name');
            $table->date('date_of_birth');
            $table->enum('migraine_frequency', ['monthly', 'weekly', 'daily']);
            $table->enum('daily_frequency', ['n/a', '1-2', '3-4', '5+']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('screening_responses');
    }
};
