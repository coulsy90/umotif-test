# uMotif-test

I have included a `README.md` file in the directories `uMotif` and `uMotif-ui` these
contain instructions on how to start the backend and the front end of the application.

I have also included a database dump in the `uMotif` directory in case there are any
issues with the setup instructions.

I have also included a screen recording of the application to demonstrate how it works.

If you have any issues please do not hesitate to contact me. 

Many thanks,
Michael

